let navBtn = document.getElementsByClassName('nav-btn')[0];
navBtn.addEventListener('click', ()=>{
    let menuIcon = document.getElementById('menu-icon');
    menuIcon.classList.toggle('fa-times');
    let dropMenu = document.getElementsByClassName('drop-menu')[0];
    dropMenu.classList.toggle('active');
});